import { ChartType, ChartOptions }    from 'chart.js'
import { Label}                       from 'ng2-charts'

export class ChartjsConfigurator {
  ready     : boolean = false
  data      : any []
  labels    : Label []
  chartType : ChartType
  options   : ChartOptions
  plugins   : any []
  colors    : any []
  legend    : boolean = true
// [Specific Logic]
  isReady () {
    return this.getReady()
  }

  prepared () : ChartjsConfigurator{
    return this.setReady(true)
  }

  unprepared () : ChartjsConfigurator{
    return this.setReady(false)
  }
// [End Specific Logic]

// [Getters]
  getData () : any [] {
    return this.data
  }

  getLabels () : Label [] {
    return this.labels
  }

  getChartType () : ChartType {
    return this.chartType
  }

  getOptions () : ChartOptions{
    return this.options
  }

  getPlugins () : any [] {
    return this.plugins
  }

  getColors () : any [] {
    return this.colors
  }

  getLegend () : boolean {
    return this.legend
  }

  getReady () : boolean {
    return this.ready
  }
// [End Getters]

// [Setters]
  setReady(ready:boolean) : ChartjsConfigurator {
    this.ready = ready

    return this
  }

  setData (data: any []) : ChartjsConfigurator {
    this.data = data

    return this
  }

  setLabels (labels : Label [] ) : ChartjsConfigurator {
    this.labels = labels

    return this
  }

  setChartType (chartType:ChartType) : ChartjsConfigurator {
    this.chartType = chartType

    return this
  }

  setOptions (options:ChartOptions) : ChartjsConfigurator {
    this.options = options

    return this
  }

  setPlugins(plugins: any []) : ChartjsConfigurator {
    this.plugins = plugins

    return this
  }

  setColors(colors: any []) : ChartjsConfigurator {
    this.colors = colors

    return this
  }

  setLegend(legend:boolean) : ChartjsConfigurator {
    this.legend = legend

    return this
  }
// [End Setters]
}
