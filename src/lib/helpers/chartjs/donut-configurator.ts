import { ChartType, ChartOptions }    from 'chart.js'
import { ChartjsConfigurator }        from '../../../../../../src/customs/wi-angular-base-classes/helpers/chartjs/chartjs-configurator'

export class DonutConfigurator extends ChartjsConfigurator{
  chartType : ChartType = 'doughnut'
}
