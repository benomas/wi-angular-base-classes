const wRandom = function (min:number, max:number) {
  return Math.random() * (max - min) + min
}

const wRandomInt = function (min:number, max:number) {
  return parseInt(wRandom(min,max).toString())
}

export {wRandom,wRandomInt}

export class Maths {
  wRandom (min:number, max:number) {
    return wRandom(min,max)
  }

  wRandomInt (min:number, max:number) {
    return wRandomInt(min,max)
  }
}

