import { Colorin } from 'wi-angular-base-classes/src/lib/helpers/colors/colorin'

export class ColorinTheme1 extends Colorin {
  protected defaultColor: string = '#f72585'
  protected colors : string []   = [
    '#4cc9f0',
    '#f72585',
    '#7209b7',
    '#b5179e',
    '#560bad',
    '#480ca8',
    '#4361ee',
    '#3a0ca3',
    '#4895ef',
    '#3f37c9'
  ]
// [Specific Logic]
// [End Specific Logic]

// [Getters]
// [End Getters]

// [Setters]
// [End Setters]
}
