import { Colorin } from 'wi-angular-base-classes/src/lib/helpers/colors/colorin'

export class ColorinTheme3 extends Colorin {
  protected defaultColor: string = '#f94144'
  protected colors : string []   = [
    '#f94144',
    '#f3722c',
    '#f8961e',
    '#f9844a',
    '#f9c74f',
    '#90be6d',
    '#43aa8b',
    '#4d908e',
    '#577590',
    '#277da1'
  ]
// [Specific Logic]
// [End Specific Logic]

// [Getters]
// [End Getters]

// [Setters]
// [End Setters]
}
