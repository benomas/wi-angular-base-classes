import { Colorin } from 'wi-angular-base-classes/src/lib/helpers/colors/colorin'

export class ColorinTheme2 extends Colorin {
  protected defaultColor: string = '#03071e'
  protected colors : string []   = [
    '#03071e',
    '#370617',
    '#6a040f',
    '#9d0208',
    '#d00000',
    '#dc2f02',
    '#e85d04',
    '#f48c06',
    '#faa307',
    '#ffba08'
  ]
// [Specific Logic]
// [End Specific Logic]

// [Getters]
// [End Getters]

// [Setters]
// [End Setters]
}
