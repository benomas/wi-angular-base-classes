import { wRandomInt }         from 'wi-angular-base-classes/src/lib/helpers/maths'
import { Colorin }            from './colorin'
import { ColorinTheme1 }      from './colorin-theme1'
import { ColorinTheme2 }      from './colorin-theme2'
import { ColorinTheme3 }      from './colorin-theme3'
import { ColorinTheme4 }      from './colorin-theme4'

export class ColorinThemes {
  protected themes:{} = {
    theme1: new ColorinTheme1(),
    theme2: new ColorinTheme2(),
    theme3: new ColorinTheme3(),
    theme4: new ColorinTheme4(),
  }

  protected defaultTheme         : Colorin
  protected currentThemePosition : number = 0
// [Specific Logic]
  nextTheme () : Colorin {
    return this.setCurrentThemePosition(this.getCurrentThemePosition() + 1).currentTheme()
  }

  previusTheme () : Colorin {
    let themePosition = this.getCurrentThemePosition() - 1

    if (themePosition < 0)
      themePosition = this.themesCount() -1

    return this.setCurrentThemePosition(themePosition).currentTheme()
  }

  currentTheme () : Colorin {
    let currentPosition = this.getCurrentThemePosition() % this.themesCount()

    if (this.getThemes()[currentPosition] != null)
      return this.getThemes()[currentPosition]

    return this.getDefaultTheme()
  }

  randomTheme () : Colorin {
    return this.getThemes()[wRandomInt(0,this.themesCount())]
  }

  fixedTheme (position) : Colorin {
    let targetPosition = this.themesCount() % position

    if(this.getThemes()[targetPosition] == null)
      return this.getDefaultTheme()

    return this.getThemes()[targetPosition]
  }

  themesCount () : number {
    return Object.keys(this.getThemes()).length
  }

  nextColor () : string {
    if(!this.currentTheme().isLastColor())
      return this.currentTheme().nextColor()

    return this.nextTheme().nextColor()
  }

  previusColor () : string {
    if(!this.currentTheme().isFirstColor())
      return this.currentTheme().previusColor()

    return this.previusTheme().previusColor()
  }

  currentColor () : string {
    return this.currentTheme().currentColor()
  }

  randomColor () : string {
    return this.randomTheme().randomColor()
  }

  fixedColor (theme:string,color:number) : string {
    if (this.getThemes()[theme] == null)
      return this.getDefaultTheme().fixedColor(color)

    return this.getThemes()[theme].fixedColor(color)
  }
// [End Specific Logic]

// [Getters]
  getThemes () :{} {
    return this.themes
  }

  getDefaultTheme () :Colorin {
    return this.defaultTheme
  }

  getCurrentThemePosition () :number {
    return this.currentThemePosition
  }
// [End Getters]

// [Setters]
  setThemes (themes:{}): ColorinThemes{
    this.themes = themes

    return this
  }

  setDefaultTheme (defaultTheme:Colorin): ColorinThemes{
    this.defaultTheme = defaultTheme

    return this
  }

  setCurrentThemePosition (currentThemePosition:number): ColorinThemes{
    this.currentThemePosition = currentThemePosition

    return this
  }
// [End Setters]
}
