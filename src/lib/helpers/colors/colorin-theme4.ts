import { Colorin } from 'wi-angular-base-classes/src/lib/helpers/colors/colorin'

export class ColorinTheme4 extends Colorin {
  protected defaultColor: string = '#eb5e28'
  protected colors : string []   = [
    '#eb5e28',
    '#f27f34',
    '#f9a03f',
    '#f6b049',
    '#f3c053',
    '#a1c349',
    '#94b33d',
    '#87a330',
    '#799431',
    '#6a8532'
  ]
// [Specific Logic]
// [End Specific Logic]

// [Getters]
// [End Getters]

// [Setters]
// [End Setters]
}
