import { wRandomInt } from 'wi-angular-base-classes/src/lib/helpers/maths'

export class Colorin {
  protected defaultColor: string          = '#FFFFFF'
  protected colors : string []            = []
  protected currentColorPosition : number = 0

  constructor() {

  }
// [Specific Logic]
  nextColor () : string {
    return this.setCurrentPositionColor(this.getCurrentColorPosition() + 1).currentColor()
  }

  previusColor () : string {
    let colorPosition = this.getCurrentColorPosition() - 1

    if (colorPosition < 0)
      colorPosition = this.colorsCount() -1

    return this.setCurrentPositionColor(colorPosition).currentColor()
  }

  currentColor () : string {
    let currentPosition = this.getCurrentColorPosition() % this.colorsCount()

    if (this.getColors()[currentPosition] != null)
      return this.getColors()[currentPosition]

    return this.getDefaultColor()
  }

  randomColor () : string {
    return this.getColors()[wRandomInt(0,this.colorsCount())]
  }

  fixedColor (position:number) : string {
    if(!this.colorsCount())
      return this.getDefaultColor()

    return this.getColors()[position % this.colorsCount()]
  }

  firstColor () : string {
    return this.fixedColor(0)
  }

  lastColor () : string {
    return this.fixedColor(this.colorsCount() - 1)
  }

  colorsCount () : number {
    return this.getColors().length
  }

  isFirstColor () : boolean {
    return this.colorsCount() % this.getCurrentColorPosition() === 0
  }

  isLastColor () : boolean {
    return this.colorsCount() % this.getCurrentColorPosition() === this.colorsCount() - 1
  }
// [End Specific Logic]

// [Getters]
  getDefaultColor () : string {
    return this.defaultColor
  }

  getColors (): string [] {
    return this.colors
  }

  getCurrentColorPosition () : number {
    return this.currentColorPosition
  }
// [End Getters]

// [Setters]
  setDefaultColor (defaultColor:string) : Colorin {
    this.defaultColor = defaultColor

    return this
  }

  setColors (colors:string []) : Colorin {
    this.colors = colors

    return this
  }

  setCurrentPositionColor (currentColorPosition:number) : Colorin {
    this.currentColorPosition = currentColorPosition

    return this
  }
// [End Setters]
}
