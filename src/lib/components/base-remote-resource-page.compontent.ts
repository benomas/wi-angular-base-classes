import { BaseResourcePage } from './base-resource-page.compontent'
import { Injector }         from 'node_modules/@angular/core'
import { environment }      from '../../../../../src/environments/environment'
import { Globals }          from '../../../../../src/app/globals'

export class BaseRemoteResourcePage extends BaseResourcePage{
  protected globals     : Globals
  protected environment : any
  protected loading     : boolean = true
  protected totalCount  : number  = 0
  protected pageSize    : number  = 50
  protected currentPage : number  = 1
  protected rows        : Array<any> = []
  protected rowMap

  constructor(protected injector:Injector) {
    super()
    this.setEnvironment(environment).setGlobals(this.inyect(Globals))
  }
// [Specific Logic]
  rowMapKeys () {
    if(this.rowMap == null)
      return []

    return  Object.keys(this.rowMap)
  }

  rowMapLabels () {
    return this.rowMapKeys().map(key=>this.colLabel(key))
  }

  colLabel (col: string = '') {
    if(this.rowMap == null || this.rowMap[col] == null)
      return ''

    return  this.rowMap[col]
  }

  loadingStart() {
    return this.setLoading(true)
  }

  loaded () {
    return this.setLoading(false)
  }

  pageJump (newPage: number) {
    this.setCurrentPage(newPage).callMainEndPoint()
  }

  lastPageIndex () {
    return Math.ceil(this.totalCount / this.pageSize)
  }

  pageRecordMessage () {
    return 'Registros por página'
  }

  pageRecordLogMessage (pagination:any) {
    let diff = (pagination.lastItem + 1) < this.getTotalCount() ? (pagination.lastItem + 1) : this.getTotalCount()

    return `${(pagination.firstItem + 1)} - ${diff} de ${this.getTotalCount()}`
  }

  noRecordsMessage () {
    return 'Sin registros que mostrar'
  }

  callMainEndPoint () : void {

  }

  onSearchClick() : void {
    this.setCurrentPage(1).callMainEndPoint()
  }

  changePage() : void {
  }

  inyect(toInyect) : any{
    return this.getInjector().get(toInyect)
  }
// [End Specific Logic]

// [Events Logic]
  get pageNav(): number {
    return this.getCurrentPage()
  }

  set pageNav(value: number) {
    if (this.getCurrentPage() !== value) {
      this.pageJump(value)
    }
  }
// [End Events Logic]

// [Getters]
  getInjector () : Injector {
    return this.injector
  }

  getEnvironment () {
    return this.environment
  }

  getGlobals () : Globals {
    return this.globals
  }

  getLoading () {
    return this.loading
  }

  getTotalCount () {
    return this.totalCount
  }

  getPageSize (){
    return this.pageSize
  }

  getCurrentPage () {
    return this.currentPage
  }

  getRowMap () {
    return this.rowMap
  }

  getRows () :Array<any> {
    return this.rows
  }
// [End Getters]

// [Setters]
  setInjector (injector:Injector) : this {
    this.injector = injector

    return this
  }

  setEnvironment (environment: any) : this {
    this.environment = environment

    return this
  }

  setGlobals (globals : Globals) : this {
    this.globals = globals

    return this
  }

  setLoading (loading:boolean) {
    this.loading = loading

    return this
  }

  setTotalCount (totalCount:number) :this {
    this.totalCount = totalCount

    return this
  }

  setPageSize (pageSize:number) :this {
    this.pageSize = pageSize

    return this
  }

  setCurrentPage (currentPage:number) :this {
    this.currentPage = currentPage

    return this
  }

  setRowMap (rowMap:any) :this {
    this.rowMap = rowMap

    return this
  }

  setRows (rows:Array<any>) : this{
    this.rows = [...rows]

    return this
  }
// [End Setters]
}
