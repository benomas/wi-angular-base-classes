import { BaseHttpRestCrudAuthService }  from '../services/base-http-rest-crud-auth.service'
import { BaseHttpRestCrudService }      from '../services/base-http-rest-crud.service'

export class BaseResourcePage{
  protected mainService   : BaseHttpRestCrudAuthService | BaseHttpRestCrudService

  getMainService () :BaseHttpRestCrudAuthService | BaseHttpRestCrudService{
    return this.mainService
  }
}
