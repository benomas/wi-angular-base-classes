import { Injector }                             from 'node_modules/@angular/core'
import { HttpClient}                            from 'node_modules/@angular/common/http'
import { Observable, of }                       from 'node_modules/rxjs'
import { CrudServiceInterface }                 from './crud-service-interface'
import { BaseHttpRestService }                  from './base-http-rest.service'
import { Method }                               from '../../../../../src/customs/wi-angular-base-classes/services/generic-configurations/method.enum'

export class BaseHttpRestCrudService extends BaseHttpRestService implements CrudServiceInterface<BaseHttpRestCrudService>{
  private rows  : Array<any> = []
  private keyBy : string     = 'id'
// [Specific Logic]
  constructor(protected httpClient: HttpClient,protected injector: Injector) {
    super(httpClient,injector)
  }
// [Specific Logic]
// [End Specific Logic]

// [EndPoints]
  show(key:any):Observable<any>{
    if(key==null)
      return of(null)

    return this.afterCall(this.beforeCall().getHttpClient().get(`${this.requestUrl()}${key}`,this.getHttpOptions().get()))
  }

  index():Observable<any>{
    return this.afterCall(this.beforeCall().getHttpClient().get(this.requestUrl(),this.getHttpOptions().get()))
  }

  store(row:any):Observable<any>{
    return this.afterCall(this.beforeCall().getHttpClient().post(this.requestUrl(),row,this.getHttpOptions().get()))
  }

  update(key:any,row:any):Observable<any>{
    if(key==null)
      return of(null)

    return this.afterCall(this.beforeCall().getHttpClient().put(`${this.requestUrl()}${key}`,row,this.getHttpOptions().get()))
  }

  delete(key:any):Observable<any>{
    if(key==null)
      return of(null)

    return this.afterCall(this.beforeCall().getHttpClient().delete(`${this.requestUrl()}${key}`,this.getHttpOptions().get()))
  }

  beforeCall () {
    return this
  }

  afterCall (response:Observable<any>) : Observable<any>{
    return response
  }

  gemericEndPoint(key:any = null, row:any = null):Observable<any>{
    if(this.getHttpMethod() === Method.GET){
      if(key)
        return this.afterCall(this.beforeCall().getHttpClient().get(`${this.requestUrl()}${key}`,this.getHttpOptions().get()))

      return this.afterCall(this.beforeCall().getHttpClient().get(this.requestUrl(),this.getHttpOptions().get()))
    }
    let genericEndPoints = {
      [Method.POST]   : () => this.afterCall(this.beforeCall().getHttpClient().post(this.requestUrl(),row,this.getHttpOptions().get())),
      [Method.PUT]    : () => this.afterCall(this.beforeCall().getHttpClient().put(`${this.requestUrl()}${key}`,row,this.getHttpOptions().get())),
      [Method.DELETE] : () => this.afterCall(this.beforeCall().getHttpClient().delete(`${this.requestUrl()}${key}`,this.getHttpOptions().get()))
    }

    if(genericEndPoints[this.getHttpMethod()] != null)
      return genericEndPoints[this.getHttpMethod()]()

    return of(null)
  }
// [End EndPoints]

// [Getters]
  getRows () {
    return this.rows
  }

  getKeyBy () {
    return this.keyBy
  }
// [End Getters]

// [Setters]
  setRows (rows:any[] = []) {
    this.rows = rows

    return this
  }

  setKeyBy (keyBy:string = 'id') {
    this.keyBy = keyBy

    return this
  }
// [End Setters]
}
