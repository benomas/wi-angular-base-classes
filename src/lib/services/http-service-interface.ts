import { HttpClient,HttpHeaders,HttpParams }    from 'node_modules/@angular/common/http'
import { HttpOptions }                          from './http-options'

export interface HttpServiceInterface<T> {
  getBaseRequestUrl () :string | null
  getHttpClient () :HttpClient
  getHttpOptions () : HttpOptions
  getOptionalUrlSegment () : string
  getBody () :any
  getHeaders () : HttpHeaders | { [header: string]: string | string[] } | null
  getObserve () : string | null
  getParams () : HttpParams | { [param: string]: string | string[] } | null
  getReportProgress () : boolean | null
  getResponseType () : string | null
  getWithCredentials () : boolean | null
  getTimeout () :number
  setBaseRequestUrl (url:string) :HttpServiceInterface<T>
  setHttpOptions (httpOptions:HttpOptions) :HttpServiceInterface<T>
  setOptionalUrlSegment (optionalUrlSegment:string) :HttpServiceInterface<T>
  setBody (body:any) :this
  setHeaders (headers:HttpHeaders | { [header: string]: string | string[] }) :HttpServiceInterface<T>
  setObserve (observe:string) :HttpServiceInterface<T>
  setParams (params:HttpParams | { [param: string]: string | string[] }) :HttpServiceInterface<T>
  setReportProgress (reportProgress : boolean) :HttpServiceInterface<T>
  setResponseType (responseType : string) :HttpServiceInterface<T>
  setWithCredentials (withCredentials : boolean) :HttpServiceInterface<T>
  setTimeout (timeout:number) :HttpServiceInterface<T>
  requestUrl () :string
  beforeCall ()  :HttpServiceInterface<T>
  httpParams (params:Object) :HttpServiceInterface<T>
  addHeader (header:string,value:string) :HttpServiceInterface<T>
}
