import { Injector } from 'node_modules/@angular/core'
export interface ConfiguratorServiceInterface<T> {
  getAppConfigWrapper () :any
  getEnv () :any
  getInjector () :Injector
  loadAppConfigWrapper () :ConfiguratorServiceInterface<T>
  loadEnv () :ConfiguratorServiceInterface<T>
  setAppConfigWrapper (appConfigWrapper:any) :ConfiguratorServiceInterface<T>
  setEnv (env:any) :ConfiguratorServiceInterface<T>
  setInjector (env:any) :ConfiguratorServiceInterface<T>
}
