export interface AuthServiceInterface<T> {
  getToken () :string
  setToken (token:string) :AuthServiceInterface<T>
}
