import { Injector }                             from 'node_modules/@angular/core'
import { HttpClient}                            from 'node_modules/@angular/common/http'
import { AuthServiceInterface }                 from 'wi-angular-base-classes/src/lib/services/auth-service-interface'
import { BaseHttpRestService }                  from './base-http-rest.service'

export class BaseHttpRestAuthService extends BaseHttpRestService implements AuthServiceInterface<BaseHttpRestAuthService>{
// [Specific Logic]
  protected token : string = ''
  constructor(protected httpClient: HttpClient,protected injector: Injector) {
    super(httpClient,injector)
  }
// [Specific Logic]
  requestUrl (): string {
    return `${super.requestUrl()}${this.getResourceSegment()}`
  }
// [End Specific Logic]

// [Getters]
  getToken () :string {
    return this.token
  }
// [End Getters]

// [Setters]
  setToken (token:string) :this {
    this.token = token

    return this
  }
// [End Setters]
}
