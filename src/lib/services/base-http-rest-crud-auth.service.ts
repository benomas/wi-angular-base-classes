import { Injector }                             from 'node_modules/@angular/core'
import { HttpClient}                            from 'node_modules/@angular/common/http'
import { AuthServiceInterface }                 from 'wi-angular-base-classes/src/lib/services/auth-service-interface'
import { BaseHttpRestCrudService }              from './base-http-rest-crud.service'

export class BaseHttpRestCrudAuthService extends BaseHttpRestCrudService implements AuthServiceInterface<BaseHttpRestCrudAuthService>{
// [Specific Logic]
  protected token : string = ''
  constructor(protected httpClient: HttpClient,protected injector: Injector) {
    super(httpClient,injector)
  }
// [Specific Logic]
// [End Specific Logic]

// [EndPoints]
// [End EndPoints]

// [Getters]
  getToken () :string {
    return this.token
  }
// [End Getters]

// [Setters]
  setToken (token:string) :this {
    this.token = token

    return this
  }
// [End Setters]
}
