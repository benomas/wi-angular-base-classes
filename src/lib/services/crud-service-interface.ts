import { Observable } from 'node_modules/rxjs'

export interface CrudServiceInterface<T> {
  getKeyBy ()                             : string|number
  getRows  ()                             : Array<any>
  setKeyBy (keyBy:string)                 : CrudServiceInterface<T>
  setRows  (rows:any[])                   : CrudServiceInterface<T>
  delete   (key:number|string)            : Observable<any>
  index    ()                             : Observable<any>
  show     (key:number|string)            : Observable<any>
  store    (row:Object)                   : Observable<any>
  update   (key:number|string,row:Object) : Observable<any>
}
