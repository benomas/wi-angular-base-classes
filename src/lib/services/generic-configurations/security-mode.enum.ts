// extendable
export enum SecurityMode {
  INTERNAL,
  EXTERNAL,
  PUBLIC,
}
