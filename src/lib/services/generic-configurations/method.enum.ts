// extendable
export enum Method {
  POST,
  GET,
  PUT,
  PATCH,
  DELETE
}
