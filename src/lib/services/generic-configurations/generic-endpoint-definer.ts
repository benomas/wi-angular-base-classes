import { Method }           from '../../../../../../src/customs/wi-angular-base-classes/services/generic-configurations/method.enum'
import { SecurityMode }    from '../../../../../../src/customs/wi-angular-base-classes/services/generic-configurations/security-mode.enum'

export class GenericEndpointDefiner {
  constructor(
    private serviceMethod : Method = Method.GET,
    private segmentUrl    : string = '',
    private isApi         : boolean = true,
    private securityMode  : SecurityMode = SecurityMode.PUBLIC,
    private waitTimeout   : number = 180000
  ) { }
// [Specific Logic]
// [End Specific Logic]

// [Getters]
  getServiceMethod() : Method{
    return this.serviceMethod
  }

  getSegmentUrl() : string{
    return this.segmentUrl
  }

  getIsApi() : boolean{
    return this.isApi
  }

  getSecurityMode() : SecurityMode{
    return this.securityMode
  }

  getWaitTimeout() :number{
    return this.waitTimeout
  }
// [End Getters]

// [Setters]
  setServiceMethod (serviceMethod: Method) : this {
    this.serviceMethod = serviceMethod

    return this
  }

  setSegmentUrl (segmentUrl: string) : this {
    this.segmentUrl = segmentUrl

    return this
  }

  setIsApi (isApi: boolean) : this {
    this.isApi = isApi

    return this
  }

  setSecurityMode (securityMode: SecurityMode) : this {
    this.securityMode = securityMode

    return this
  }

  setWaitTimeout (waitTimeout: number) : this {
    this.waitTimeout = waitTimeout

    return this
  }
// [End Setters]
}
