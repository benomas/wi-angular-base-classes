export interface RestServiceInterface<T> {
  getResource () :string
  getResourceSegment () :string
  setResource (resource:string) :RestServiceInterface<T>
  setResourceSegment (resourceSegment:string) :RestServiceInterface<T>
}
