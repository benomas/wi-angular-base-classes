import { Injector }                             from 'node_modules/@angular/core'
import { HttpClient, HttpHeaders, HttpParams }  from 'node_modules/@angular/common/http'
import { HttpServiceInterface }                 from './http-service-interface'
import { ConfiguratorServiceInterface }         from './configurator-service-interface'
import { HttpOptions }                          from './http-options'
import { environment }                          from '../../../../../src/environments/environment'
import { Globals }                              from '../../../../../src/app/globals'
import { Method }                               from '../../../../../src/customs/wi-angular-base-classes/services/generic-configurations/method.enum'

export class BaseHttpService implements ConfiguratorServiceInterface<BaseHttpService>,HttpServiceInterface<BaseHttpService>{
  protected baseRequestUrl     : string | null = null
  protected optionalUrlSegment : string = ''
  protected appConfigWrapper   : any
  protected env                : any
  protected httpOptions        : HttpOptions
  protected timeout            : number = 40000
  protected httpMethod         : Method = Method.GET
// [Specific Logic]
  constructor(protected httpClient: HttpClient,protected injector: Injector) {
    this.loadEnv()
      .loadAppConfigWrapper()
      .fixBaseUrl()
      .setHttpOptions(new HttpOptions())
      .setHeaders(new HttpHeaders())
      .setReportProgress(false)
      .setResponseType('json')
      .setWithCredentials(false)
      .setTimeout(this.getEnv().ajaxTimeOut || this.getTimeout())
  }
// [Specific Logic]
  loadAppConfigWrapper () :this{
    return this.setAppConfigWrapper(this.getInjector().get(Globals))
  }

  loadEnv () :this{
    return this.setEnv(environment)
  }

  // this method beheavior needs to be final, so dont rewrite it
  rootRequestUrl (): string {
    return `${this.getBaseRequestUrl()}${this.getOptionalUrlSegment()}`
  }

  requestUrl (): string {
    return this.rootRequestUrl()
  }

  beforeCall () {
    return this.fixBaseUrl()
      .setHttpOptions(new HttpOptions())
      .setHeaders(new HttpHeaders())
      .setReportProgress(false)
      .setResponseType('json')
      .setWithCredentials(false)
      .setTimeout(this.getEnv().ajaxTimeOut || this.getTimeout())
  }

  httpParams (params:Object) :this {
    this.getHttpOptions().httpParams(params)

    return this
  }

  addHeader(header,value) :this {
    this.getHttpOptions().addHeader(header,value)

    return this
  }

  removeHeader(header) :this {
    this.getHttpOptions().removeHeader(header)

    return this
  }

  refreshHeader(header,value) :this {
    this.getHttpOptions().refreshHeader(header,value)

    return this
  }

  fixBaseUrl () : this{
    if(!this.getBaseRequestUrl()){
      if(!this.getEnv() || this.getEnv().serverUrl == null)
        this.setBaseRequestUrl('/api/')
      else
        this.setBaseRequestUrl(this.getEnv().serverUrl)
    }

    return this
  }
// [End Specific Logic]

// [Getters]
  getAppConfigWrapper () :any{
    return this.appConfigWrapper
  }

  getEnv () :any{
    return this.env
  }

  getInjector () :Injector{
    return this.injector
  }

  getBaseRequestUrl () :string | null{
    return this.baseRequestUrl
  }

  getHttpClient () :HttpClient{
    return this.httpClient
  }

  getHttpOptions () : HttpOptions {
    return this.httpOptions
  }

  getOptionalUrlSegment () :string{
    return this.optionalUrlSegment
  }

  getBody () :any {
    return this.getHttpOptions().getBody()
  }

  getHeaders () :HttpHeaders | { [header: string]: string | string[] } | null {
    return this.getHttpOptions().getHeaders()
  }

  getObserve () :string | null  {
    return this.getHttpOptions().getObserve()
  }

  getParams () :HttpParams | { [param: string]: string | string[] } | null {
    return this.getHttpOptions().getParams()
  }

  getReportProgress () :boolean | null {
    return this.getHttpOptions().getReportProgress()
  }

  getResponseType () :string | null {
    return this.getHttpOptions().getResponseType()
  }

  getWithCredentials () :boolean | null {
    return this.getHttpOptions().getWithCredentials()
  }

  getTimeout () :number {
    return this.timeout
  }

  getHttpMethod () : Method {
    return this.httpMethod
  }
// [End Getters]

// [Setters]
  setAppConfigWrapper (appConfigWrapper:any) :this{
    this.appConfigWrapper = appConfigWrapper

    return this
  }

  setEnv (env :any) :this{
    this.env = env

    return this
  }

  setInjector (injector :Injector) :this{
    this.injector = injector

    return this
  }

  setBaseRequestUrl (baseRequestUrl:string = '') :BaseHttpService{
    this.baseRequestUrl = baseRequestUrl

    return this
  }

  setHttpOptions (httpOptions:HttpOptions) : this{
    this.httpOptions = httpOptions

    return this
  }

  setOptionalUrlSegment (optionalUrlSegment:string) :this{
    this.optionalUrlSegment = optionalUrlSegment

    return this
  }

  setBody (body:any) :this {
    this.getHttpOptions().setBody(body)

    return this
  }

  setHeaders (headers:HttpHeaders | { [header: string]: string | string[] }) :this {
    this.getHttpOptions().setHeaders(headers)

    return this
  }

  setObserve (observe:string) :this {
    this.getHttpOptions().setObserve(observe)

    return this
  }

  setParams (params:HttpParams | { [param: string]: string | string[] }) :this {
    this.getHttpOptions().setParams(params)

    return this
  }

  setReportProgress (reportProgress:boolean) :this {
    this.getHttpOptions().setReportProgress(reportProgress)

    return this
  }

  setResponseType (responseType : string) :this {
    this.getHttpOptions().setResponseType(responseType)

    return this
  }

  setWithCredentials (withCredentials : boolean) :this {
    this.getHttpOptions().setWithCredentials(withCredentials)

    return this
  }

  setTimeout (timeout:number) :this {
    this.timeout = timeout

    return this
  }

  setHttpMethod(httpMethod : Method) : this{
    this.httpMethod = httpMethod

    return this
  }
// [End Setters]
}
