import { Injector }                             from 'node_modules/@angular/core'
import { HttpClient }                           from 'node_modules/@angular/common/http'
import { RestServiceInterface }                 from './rest-service-interface'
import { BaseHttpService }                      from './base-http.service'

export class BaseHttpRestService extends BaseHttpService implements RestServiceInterface<BaseHttpRestService>{
  protected resource        : string      = ''
  protected resourceSegment : string      = ''
// [Specific Logic]
  constructor(protected httpClient: HttpClient,protected injector: Injector) {
    super(httpClient,injector)
  }
// [Specific Logic]
  requestUrl (): string {
    return `${super.requestUrl()}${this.getResourceSegment()}`
  }
// [End Specific Logic]

// [Getters]
  getResource () :string{
    return this.resource
  }

  getResourceSegment () :string{
    return this.resourceSegment
  }
// [End Getters]

// [Setters]
  setResource (resource:string) :this{
    this.resource = resource

    return this
  }

  setResourceSegment (resourceSegment:string) :this{
    this.resourceSegment = resourceSegment

    return this
  }
// [End Setters]
}
