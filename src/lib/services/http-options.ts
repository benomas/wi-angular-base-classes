import { HttpHeaders, HttpParams }  from 'node_modules/@angular/common/http'

export class HttpOptions{
  private body           : any                                                   = null
  private headers        : HttpHeaders | { [header: string]: string | string[] } = null
  private observe        : string                                                = null
  private params         : HttpParams | { [param: string]: string | string[] }   = null
  private reportProgress : boolean                                               = null
  private responseType   : string                                                = null
  private withCredentials: boolean                                               = null

// [Specific Logic]
  httpParams (params:Object) :this{
    let httpParams = new HttpParams()
    let paramsCacher

    for (const [dataItem, dataValue] of Object.entries(params)){
      if(typeof paramsCacher === 'undefined')
        paramsCacher = httpParams.set(dataItem, String(dataValue))
      else
        paramsCacher = paramsCacher.set(dataItem, String(dataValue))
    }

    return this.setParams(paramsCacher)
  }

  addHeader(header : string,value : any){
    if(this.getHeaders() instanceof HttpHeaders)
      return this.setHeaders((<HttpHeaders> this.getHeaders()).append(header,value))

    this.getHeaders()[header] = value

    return this
  }

  removeHeader(header : string){
    if(this.getHeaders() instanceof HttpHeaders){
      let httpHeaders:HttpHeaders = <HttpHeaders> this.getHeaders()

      if(httpHeaders.has(header))
        return this.setHeaders((<HttpHeaders> this.getHeaders()).delete(header))

      return this
    }

    let headers = this.getHeaders()

    if (headers[header] != null)
      delete headers[header]

    return this.setHeaders(headers)
  }

  refreshHeader(header : string,value : any){
    return this.removeHeader(header).addHeader(header,value)
  }
// [End Specific Logic]

// [Getters]
  getBody () :any {
    return this.body
  }

  getHeaders () :HttpHeaders | { [header: string]: string | string[] } | null {
    return this.headers
  }

  getObserve () :string | null {
    return this.observe
  }

  getParams () :HttpParams | { [param: string]: string | string[] } | null {
    return this.params
  }

  getReportProgress () :boolean | null {
    return this.reportProgress
  }

  getResponseType () :string | null {
    return this.responseType
  }

  getWithCredentials () :boolean | null {
    return this.withCredentials
  }

  get () {
    const dinamicOption = function (property:string = null,value:any = null) {
      if (property == null || value == null)
        return {}

      return {[property]:value}
    }

    return {
      ...dinamicOption('headers',this.getHeaders()),
      ...dinamicOption('observe',this.getObserve()),
      ...dinamicOption('params',this.getParams()),
      ...dinamicOption('reportProgress',this.getReportProgress()),
      ...dinamicOption('responseType',this.getResponseType()),
      ...dinamicOption('withCredentials',this.getWithCredentials()),
    }
  }
// [End Getters]

// [Setters]
  setBody (body : any) :this {
    this.body = body

    return this
  }

  setHeaders (headers:HttpHeaders | { [header: string]: string | string[] }) :this {
    this.headers = headers

    return this
  }

  setObserve (observe:string) :this {
    this.observe = observe

    return this
  }

  setParams (params:HttpParams | { [param: string]: string | string[] }) :this {
    this.params = params

    return this
  }

  setReportProgress (reportProgress : boolean) :this {
    this.reportProgress = reportProgress

    return this
  }

  setResponseType (responseType : string) :this {
    this.responseType = responseType

    return this
  }

  setWithCredentials (withCredentials : boolean) :this {
    this.withCredentials = withCredentials

    return this
  }
// [End Setters]
}
