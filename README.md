# WiAngularBaseClasses

This library was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.2.4.

## Code scaffolding

Run `ng generate component component-name --project wi-angular-base-classes` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module --project wi-angular-base-classes`.
> Note: Don't forget to add `--project wi-angular-base-classes` or else it will be added to the default project in your `angular.json` file. 

## Build

Run `ng build wi-angular-base-classes` to build the project. The build artifacts will be stored in the `dist/` directory.

## Publishing

After building your library with `ng build wi-angular-base-classes`, go to the dist folder `cd dist/wi-angular-base-classes` and run `npm publish`.

## Running unit tests

Run `ng test wi-angular-base-classes` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
